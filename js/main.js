var totales = 0;
var subtotales = 0;
var comisiones = 0;

function change(){
        const monedaOrigen = document.getElementById('monedaOrigen');
        const monedaDestino = document.getElementById('monedaDestino');
        let mostrar = `<option value="0" disabled selected>Moneda Destino</option>`;
    
        switch (monedaOrigen.value) {
            case "1":
                mostrar += '<option value="2">Dólar Canadiense</option> <option value="3">Peso Mexicano</option><option value="4">Euro</option>';
                break;
    
            case "2":
                mostrar += '<option value="1">Dólar Estadounidense</option><option value="3">Peso Mexicano</option><option value="4">Euro</option>';
                break;
    
            case "3":
                mostrar += '<option value="1">Dólar Estadounidense</option><option value="2">Dólar Canadiense</option><option value="4">Euro</option>'
                break;
    
            case "4":
                mostrar += '<option value="1">Dólar Estadounidense</option><option value="2">Dólar Canadiense</option><option value="3">Peso Mexicano</option>';
                break;
    
            default:
                return alert("Valor inválido. Introduzca un valor válido.");
        }
    
        monedaDestino.innerHTML = mostrar;
}

function calcular(){
    let cantidad = parseFloat(document.getElementById('cantidad').value);
    let comision = document.getElementById('totalComision').value;
    let subtotal = document.getElementById('subtotal').value;
    let total = document.getElementById('totalPagar').value;
    let monOrigen = document.getElementById('monedaOrigen').value;
    let monDestino = document.getElementById('monedaDestino').value;
    if (isNaN(cantidad) || cantidad < 0) {
		document.getElementById('cantidad').value = "";
		return alert("Introduzca una cantidad valida");
	}
    switch(monOrigen){
        case '1': //dolar USD
            switch(monDestino){
                case '2':
                    subtotal = cantidad * 1.35;
                    break;
                case '3':
                    subtotal = cantidad * 19.85;
                    break;
                 case '4':
                    subtotal = cantidad * 0.99;
                    break;
            }
            break;
        case '2': //dolar Canadiense
            switch(monDestino){
                case '1':
                    subtotal = (cantidad /1.35);
                    break;
                case '3':
                    subtotal = (cantidad /1.35)*19.85;
                    break;
                 case '4':
                    subtotal = (cantidad /1.35)* 0.99;
                    break;
            }
            break;
        case '3': //peso mexicano
            switch(monDestino){
                case '1':
                    subtotal = (cantidad /19.85) ;
                    break;
                case '2':
                    subtotal = (cantidad /19.85) * 1.35 ;
                    break;
                 case '4':
                    subtotal = (cantidad /19.85) * 0.99;
                    break;
            }
            break;
        case '4':
            switch(monDestino){
                case '1':
                    subtotal = (cantidad /0.99);
                    break;
                case '2':
                    subtotal = (cantidad /0.99)* 1.35;
                    break;
                case '3':
                    subtotal = (cantidad /0.99)* 19.85;
                    break;
            }
            break;
        default:
            break;
    }
    comision = subtotal * 0.03;
    total = subtotal + comision;
    document.getElementById('subtotal').value = (subtotal).toFixed(2);
    document.getElementById('totalComision').value = (comision).toFixed(2);
    document.getElementById('totalPagar').value = (total).toFixed(2);
}

function registrar(){
    //todos los objetos
    if (document.getElementById('subtotal').value === "") {
		calcular();
	}
    let contenidoTabla = document.getElementById('contenidoTabla'); 
    let cantidad = document.getElementById('cantidad').value;
    let comision = document.getElementById('totalComision').value;
    let subtotal = document.getElementById('subtotal').value;
    let total = document.getElementById('totalPagar').value;
    let monOrigen = document.getElementById('monedaOrigen').value;
    let monDestino = document.getElementById('monedaDestino').value;
    totales += parseFloat(total);
    subtotales += parseFloat(subtotal);
    comisiones += parseFloat(comision);
    document.getElementById('totales').innerHTML = '$' + totales;
    document.getElementById('subtotales').innerHTML = '$' + subtotales;
    document.getElementById('comisiones').innerHTML = '$' + comisiones;
    function StringMoneda(valor){
        switch(valor){
            case '1':
                return 'Dólar Estadounidense';
            case '2':
                return 'Dólar Canadiense';
            case '3':
                return 'Pesos Mexicanos';
            case '4':
                return 'Euro';
            default:
                return null;
        }
    }
    monOrigen = StringMoneda(monOrigen);
    monDestino = StringMoneda(monDestino);
    contenidoTabla.innerHTML += `<p>${cantidad}</p> <p>${monOrigen}</p> <p>${monDestino}</p> <p>${subtotal}</p> <p>${comision}</p> <p>${total}</p>` /*Template string para poder hacer textos muy largos*/
}

function borrar(){
    contenidoTabla.innerHTML = "";
    comisiones = 0;
    subtotales = 0;
    totales = 0;
    document.getElementById('comisiones').innerHTML = '$0.00';
    document.getElementById('subtotales').innerHTML = '$0.00';
    document.getElementById('totales').innerHTML = '$0.00';
}

document.getElementById('monedaOrigen').addEventListener('change',change);
